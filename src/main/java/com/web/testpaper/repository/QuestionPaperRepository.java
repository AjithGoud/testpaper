package com.web.testpaper.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.web.testpaper.model.QuestionPaper;

@Repository
public interface QuestionPaperRepository extends JpaRepository<QuestionPaper, Integer>{
	@Query(value="select * from question where test_series=:testSeries",nativeQuery = true)
	List<QuestionPaper> findByTestSeries(@Param("testSeries")String testSeries);

}
