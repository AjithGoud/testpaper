package com.web.testpaper.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.testpaper.model.Answeres;

public interface AnswerRepository extends JpaRepository<Answeres, Integer> {

}
