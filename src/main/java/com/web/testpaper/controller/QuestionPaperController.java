 package com.web.testpaper.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.web.testpaper.exception.ResourceNotFoundException;
import com.web.testpaper.model.QuestionPaper;
import com.web.testpaper.repository.QuestionPaperRepository;

@RestController
@RequestMapping(value = "/api/v1")
public class QuestionPaperController {

	@Autowired
	private QuestionPaperRepository questionPaperRepo;

	@GetMapping(value = "/QuestionPaper/findAll")
	public ResponseEntity<List<QuestionPaper>> findAll() throws ResourceNotFoundException {
		List<QuestionPaper> listPaper = questionPaperRepo.findAll();
		if (listPaper.isEmpty()) {
			throw new ResourceNotFoundException("No Questions Availaible");
		}
		return ResponseEntity.ok().body(listPaper);

	}
	@GetMapping(value = "/QuestionPaper/getBySeries")
	public ResponseEntity<List<QuestionPaper>> getByQuestionType(@RequestParam String testSeries) throws ResourceNotFoundException {
		List<QuestionPaper> listPaper = questionPaperRepo.findByTestSeries(testSeries);
		if (listPaper.isEmpty()) {
			throw new ResourceNotFoundException("No Questions Availaible");
		}
		return ResponseEntity.ok().body(listPaper);

	}

	@SuppressWarnings("unused")
	@GetMapping(value = "/QuestionPaper/create")
	public ResponseEntity<String> create(@RequestBody MultipartFile file)
			throws ResourceNotFoundException, IOException {
		InputStream inputFile = file.getInputStream();
		XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Row row;

		for (int i = 1; i <= sheet.getLastRowNum(); i++) {// points to the starting of excel i.e excel first row

			row = (Row) sheet.getRow(i); // sheet number

			String question;
			if (row.getCell(1) == null) {
				question = null;
			} else
				question = row.getCell(1).toString();

			String choice1;
			if (row.getCell(2) == null) {
				choice1 = null;
			} else
				choice1 = row.getCell(2).toString();

			String choice2;

			if (row.getCell(3) == null) {
				choice2 = null;
			} else
				choice2 = row.getCell(3).toString();

			String choice3;

			if (row.getCell(4) == null) {
				choice3 = null;
			} else
				choice3 = row.getCell(4).toString();

			String choice4 = null;

			if (row.getCell(5) == null) {
				choice4 = null;
			} else
				choice4 = row.getCell(5).toString();

			String correctAnswere;

			if (row.getCell(6) == null) {
				correctAnswere = null;
			} else
				correctAnswere = row.getCell(6).toString();

			String testSeries;

			if (row.getCell(7) == null) {
				testSeries = null;
			} else
				testSeries = row.getCell(7).toString();

			String questionMarks;

			if (row.getCell(8) == null) {
				questionMarks = null;
			} else
				questionMarks = row.getCell(8).toString();

			QuestionPaper quest = new QuestionPaper();
			quest.setQuestion(question);
			quest.setChoice1(choice1);
			quest.setChoice2(choice2);
			quest.setChoice3(choice3);
			quest.setChoice4(choice4);
			quest.setCorrectAnswere(correctAnswere);
			quest.setTestSeries(testSeries);
			quest.setQuestionMarks(questionMarks);
			questionPaperRepo.save(quest);
		}

		return ResponseEntity.ok().body("Question Paper Uploaded");

	}

	@PutMapping(value = "/QuestionPaper")
	public ResponseEntity<QuestionPaper> update(@RequestBody QuestionPaper question) throws ResourceNotFoundException {
		QuestionPaper quest = questionPaperRepo.findById(question.getQuestionId()).get();
		BeanUtils.copyProperties(question, quest);
		questionPaperRepo.save(quest);
		return ResponseEntity.ok().body(questionPaperRepo.save(question));
	}

	@DeleteMapping(value = "/QuestionPaper/remove")
	public ResponseEntity<String> deleteQuestion(@RequestBody QuestionPaper question) {
		questionPaperRepo.delete(question);
		return ResponseEntity.ok().body("Question Deleted");
	}
}
