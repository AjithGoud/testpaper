package com.web.testpaper.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "question")
public class QuestionPaper {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("question_id")
	@Column(name="question_id")
	private int questionId;
	@JsonProperty("question")
	@Column(name="question")
	private String question;
	@JsonProperty("choice_1")
	@Column(name="choice_1")
	private String choice1;
	@JsonProperty("choice_2")
	@Column(name="choice_2")
	private String choice2;
	@JsonProperty("choice_3")
	@Column(name="choice_3")
	private String choice3;
	@JsonProperty("choice_4")
	@Column(name="choice_4")
	private String choice4;
	@JsonProperty("correct_answere")
	@Column(name="correct_answere")
	private String correctAnswere;
	@JsonProperty("test_series")
	@Column(name="test_series")
	private String testSeries;
	@JsonProperty("question_marks")
	@Column(name="question_marks")
	private String questionMarks;
	 

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getChoice1() {
		return choice1;
	}

	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return choice2;
	}

	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return choice3;
	}

	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public String getChoice4() {
		return choice4;
	}

	public void setChoice4(String choice4) {
		this.choice4 = choice4;
	}

	public String getCorrectAnswere() {
		return correctAnswere;
	}

	public void setCorrectAnswere(String correctAnswere) {
		this.correctAnswere = correctAnswere;
	}

	public String getTestSeries() {
		return testSeries;
	}

	public void setTestSeries(String testSeries) {
		this.testSeries = testSeries;
	}

	public String getQuestionMarks() {
		return questionMarks;
	}

	public void setQuestionMarks(String questionMarks) {
		this.questionMarks = questionMarks;
	}

	@Override
	public String toString() {
		return "QuestionPaper [questionId=" + questionId + ", question=" + question + ", choice1=" + choice1
				+ ", choice2=" + choice2 + ", choice3=" + choice3 + ", choice4=" + choice4 + ", correctAnswere="
				+ correctAnswere + ", testSeries=" + testSeries + ", questionMarks=" + questionMarks + "]";
	}

}
