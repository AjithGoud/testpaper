package com.web.testpaper.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "answer")
public class Answeres {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("answer_id")
	@Column(name="answer_id")
	private int ansereId;
	@Column(name="question_fk")
	private int questionFk;
	@Column(name="answere")
	private String answere;
	@Column(name="user_name")
	private String userName;

	public int getAnsereId() {
		return ansereId;
	}

	public void setAnsereId(int ansereId) {
		this.ansereId = ansereId;
	}

	public int getQuestionFk() {
		return questionFk;
	}

	public void setQuestionFk(int questionFk) {
		this.questionFk = questionFk;
	}

	public String getAnswere() {
		return answere;
	}

	public void setAnswere(String answere) {
		this.answere = answere;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "Answeres [ansereId=" + ansereId + ", questionFk=" + questionFk + ", answere=" + answere + ", userName="
				+ userName + "]";
	}

}
