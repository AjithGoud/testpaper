package com.web.testpaper.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Resource Not Found") // 404
public class ResourceNotFoundException extends Exception {

	private static final long serialVersionUID = -6007334204499522573L;

	public ResourceNotFoundException(String message) {
		super(message);
	}
}
